<?php get_header() ?>
<section class="subhero-section blue-degree-bg">
    <div class="container header-image">
        <div class="row">
            <div class="subhero">

                <div class="banner-experts-small">
                    <p><?php echo get_field('headline'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="submenucontainer">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <nav class="submenu">
                    <ul class="columns">
                        <li class="back-button">
                            <a href="/nemzetkozi-orvosi-szakvelemeny/">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Vissza
                            </a>
                        </li>
                        <?php wp_nav_menu(
                            array(
                                'menu' => 'nemzetközi_orvosi_szakvelemeny_submenu',
                                'menu_container' => '',
                                'theme_location' => 'nemzetközi_orvosi_szakvelemeny_submenu',
                                'container' => false,
                                'items_wrap' => '%3$s',
                                'container_class' => false,
                                'container_id' => '',
                                'menu_class' => 'menu',
                                'fallback_cb' => 'wp_page_menu',
                                'before' => '',
                                'after' => '',
                                'link_before' => '',
                                'link_after' => '',
                                'walker' => '',
                            )
                        ); ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="sensor"></div>
<div class="sensortwo"></div>

<section id="subpagecontent" class="orvosi-szakvelemeny">
    <div class="content-wrapp">
        <div class="wrapp">
            <div id="expert-infofaq">

                <div id="expert-infofaq-txt" class="blue" style="text-align: center">
                    <span class="dark-blue">A Második Orvosi Vélemény</span> az Ön betegségének kezelésére
                    specializálódott orvosok szaktudásához
                    biztosít hozzáférést,
                    akik személyre szabott véleményt és kezelési javaslatot adnak az elsődleges diagnózisa és
                    kezelési terve alapján.
                </div>
            </div>

            <div class="expert-info-title dark-blue">
                A MÁSODIK ORVOSI VÉLEMÉNY
            </div>
            <div class="expert-info-title-txt">
                <div class="expert-info-title-txt-left dark-grey">
                    A Második Orvosi Vélemény keretében a világ vezető szakorvosai vizsgálják felül az Ön állapotát,
                    hogy a kezelés az orvoslás elérhető legmagasabb fokán történjen.
                    Helyi és nemzetközi szakemberekkel dolgozva részletes szakvéleményt állítunk össze a
                    betegségéről és
                    az ajánlott kezelési módokról. A kapcsolattartást az Ön számára kijelölt esetfelelős orvos
                    magyar
                    nyelven biztosítja. Az esetfelelős - aki tehát maga is szakképzett orvos - kórtörténetét és a
                    kezelés minden szempontját figyelembe tanácsot ad Önnek, és segít az elkészült szakvélemény
                    értelmezésében és felhasználásában.
                    A szakvélemény az Ön leletei, és az esetfelelős orvos által összegyűjtött orvosi infomáció
                    alapján
                    készül el. A program segítségével rövid időn belül megtudhatja, mit gondolnak állapotáról a
                    világ
                    vezető szakértői, lehetővé téve, hogy megalapozott döntést hozzon a következő lépésről - mindezt
                    pedig anélkül, hogy el kellene hagynia az otthonát!
                </div>

            </div>


        </div>
    </div>

    <div class="content-wrapp blue-title-bg" style="height: 53px;">
        <div class="wrapp">


        </div>
    </div>

    <div class="content-wrapp">
        <div class="wrapp">
            <div class="expert-info-title dark-blue" style="width: 100% !important;">
                HOGYAN ZAJLIK A FOLYAMAT?
            </div>
            <div class="col-md-3">
                <div class="title dark-blue"><h3>Személyes asszisztencia</h3></div>

                <p>
                    Kijelölünk egy esetfelelős orvost, aki a teljes folyamaton végigkalauzolja Önt az elsőtől az utolsó
                    lépésig.
                </p>

            </div>
            <div class="col-md-3">
                <div class="title dark-blue"><h3>Orvosi dokumentációk</h3></div>
                <p>
                    Összegyűjtjük az Ön teljes orvosi dokumentációját, és az alapján angol nyelvű összefoglalót
                    készítünk a betegségéről!
                </p>
            </div>
            <div class="col-md-3">
                <div class="title dark-blue"><h3>Kiválasztjuk a megfelelő szakértőket</h3></div>

                <p>
                    A Teladoc klinikai tanácsa megvizsgálja az Ön esetét, és ennek alapján kiválasztja az Ön
                    esetének megfelelő szakértőket.
                </p>
            </div>
            <div class="col-md-3">
                <div class="title dark-blue"><h3>Véleményezés</h3></div>

                <p>
                    Néhány héten vagy napon belül megkapja a szakértők által összeállított szakvéleményt, benne a
                    rendelkezésre álló alternatív kezelések leírásával, és a kérdéseire adott válaszokkal."
                </p>
            </div>

        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="content-wrapp blue-title-bg">
        <div class="wrapp">

            <div id="expertopinion-box">

                <div id="expert-opinion-img">

                </div>

                <div class="expertopinion-title">
                    MI SZÜKSÉG VAN A MÁSODIK ORVOSI VÉLEMÉNYRE?
                </div>
                <div class="expertopinion-subtitle dark-blue">
                    ...mert mindent képes megváltoztatni:
                </div>
                <div class="opinion-slider-wrapp">
                    <div class="opinion-slider">
                        <div id="opslide" class="oslide">
                            <div class="opinion-slider-txt">
                                “Olyan, mintha egy saját, személyre szabott orvosom lenne. Az Esetmenedzserem nagyon
                                proaktív volt, és állandó kapcsolatban álltam vele. Biztos akartam lenni, hogy
                                minden lehetőséget felderítettünk, mielőtt műtétre került volna a sor, és az Orvosi
                                szakvélemény végül nagyon megnyugtatott.”
                            </div>
                            <div class="opinion-slider-subtxt dark-blue">

                            </div>
                        </div>
                    </div>
                </div>
                <!--
                <div id="opinion-points">
                    <ul>
                        <li id="opinion-bot1" class="active"></li>
                        <li id="opinion-bot2"></li>
                    </ul>
                </div>
                -->

            </div>

        </div>
    </div>

    <div class="content-wrapp">
        <div class="wrapp">

            <div class="expert-info-title dark-blue">
                A SZÁMOK, AMIK FONTOSAK ÜGYFELEINKNEK
            </div>

            <div id="circles-box">
                <div class="contact-box-content cb1 static">
                    <div class="circle-wrapp">
                        <div class="circle1">
                            <div class="contact-box circle">
                                <div class="percent-txt blue-sea">
                                    <span class="percent2 blue-sea" data-percent="7">7<span>%</span></span>
                                    Szakértőink az eseték 7%-ában változtattak az eredeti diagnózison
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="circle-shadow"></div>
                </div>


                <div class="contact-box-content cb2">
                    <div class="circle-wrapp">
                        <div class="circle2">
                            <div class="contact-box circle">
                                <div class="percent-txt blue-sea">
                                    <span class="percent blue-sea" data-percent="21">21<span>%</span></span>
                                    Szakértőink az esetek 21%-ában javasolja a kezelés megváltoztatását
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="circle-shadow"></div>
                </div>

                <div class="contact-box-content cb3">
                    <div class="circle-wrapp">
                        <div class="circle3">
                            <div class="contact-box circle">
                                <div class="percent-txt blue-sea">
                                    <span class="percent blue-sea" data-percent="20">20<span>%</span></span>
                                    Pácienseink 20% -a került el felesleges műtéteket
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="circle-shadow"></div>
                </div>

                <div class="contact-box-content cb4">
                    <div class="circle-wrapp">
                        <div class="circle4">
                            <div class="contact-box circle">
                                <div class="percent-txt blue-sea">
                                    <span class="percent blue-sea" data-percent="98">99<span>%</span></span>
                                    Pácienseink 99%-a elégedett a szolgáltatásunkkal és a Teladoct ajánlja
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="circle-shadow"></div>
                </div>


            </div>


            <div class="circles-subtitle blue-sea">

            </div>

        </div>
    </div>
    <div class="center-footer-smartphone">
        <div class="content-wrapp footer-bg">
            <div class="wrapp">


            </div>
        </div>
    </div>

</section>
<h6>.</h6>

<?php get_footer() ?>
