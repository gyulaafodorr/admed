<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage admed
 * @since admed 8.6
 */
?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="footer-left">
                    <div id="footer-title"></div>

                    <nav id="footermenu-left">
                        <?php wp_nav_menu(
                            array(
                                'theme_location' => 'header_menu',
                                'menu' => 'header_menu',
                                'menu_container' => 'ul',
                            )
                        ); ?>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <hr class="footerhr">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <nav class="footer-submenu">
                    <ul>
                        <?php wp_nav_menu(
                            array(
                                'menu' => 'footer_rolunk_kapcsolat',
                                'menu_container' => '',
                                'theme_location' => 'footer_rolunk_kapcsolat',
                                'container' => false,
                                'items_wrap' => '%3$s',
                                'container_class' => false,
                                'container_id' => '',
                                'menu_class' => 'menu',
                                'fallback_cb' => 'wp_page_menu',
                                'before' => '',
                                'after' => '',
                                'link_before' => '',
                                'link_after' => '',
                                'walker' => '',
                            )
                        ); ?>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6">
                <div class="copiright">
                    <span><img src="<?php bloginfo('template_url'); ?>/img/logo-teladoc-horiz.png"
                               alt="teladoc" class="footer-logo"/></span><br>
                    <span> © <?php echo date('Y') ?> Budapest - Minden jog fenntartva</span>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

<?php if (get_post_field('ID', get_post()) === 9): ?>
    <script src="<?php bloginfo('template_url'); ?>/js/google-init-map.js"></script>

    <!--innit map-->
    <script async="" defer=""
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYXMnKfZQ37k0NLLkdjSxN9zOkdYTZCis&amp;callback=initMap"
            type="text/javascript"></script>

    <!-- marker clusterer -->
    <script src="<?php bloginfo('template_url'); ?>/js/markerclusterer.js"></script>
<?php endif; ?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php bloginfo('template_url'); ?>/bootstrap/js/bootstrap.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>

<!-- dropdown menu open (hover) -->
<script>
    //    jQuery('ul.nav li.dropdown').hover(function() {
    //        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    //    }, function() {
    //        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    //    });
    <!-- scroll top button -->
    $('a[href=\'#top\']').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return false;
    });
    <!-- bgimare resize -->
    //    type="text/javascript">
    //    jQuery(document).ready(function()
    //    {
    //        imgResizer();
    //        $( window ).resize(function()
    //        {
    //            imgResizer();
    //        });
    //        function imgResizer()
    //        {
    //            $('.bgimg').each(function() {
    //                var w = $(this).width();
    //                $(this).height(w/2.13);
    //            });
    //        }
    //    });
    <!-- top menu bacgrund color -->
    //    (function ($) {
    //        $(document).ready(function () {
    //            $(window).scroll(function () {
    //                var w = $('.bgimg').height();
    //                if ($(document).scrollTop() < w - 60) {
    //                    $('nav.navbar').removeClass('navbar-fixed-top mainmenu').addClass('navbar-fixed-top');
    //                } else {
    //                    $('nav.navbar').removeClass('navbar-fixed-top').addClass('navbar-fixed-top mainmenu');
    //                }
    //            });
    //        });
    //    }) (jQuery);

</script>
</body>
</html>
