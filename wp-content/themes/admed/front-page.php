<?php get_header(); ?>

    <!--front page start hero-->
    <div class="hero">
        <div class="overlay"></div>
        <div class="slogen">
            <img src="<?php bloginfo('template_url'); ?>/img/teladochealth_logo_plum+aqua_rgb.png"
                 alt="Logo Teladoc">

            <p>We bring the world's medical experts within arms reach</p></div>

    </div>
    <div class="clearfix herobottom"></div>
    <!--/hero-->

    <section id="our-vision">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="box-1-left">
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="conzainer-box1">
                        <h3 class="home-box-title box-1-right">
                            MIRE TÖREKSZÜNK?
                        </h3>
                        <span class="home-separador"></span>

                        <p class="home-box-txt dark-grey">
                            A célunk, hogy pácienseink a <span
                                    class="dark-blue">világszerte legelismertebb szakorvosok</span> véleményének
                            segítségével dönthessenek egészségükről.
                        </p>

                        <div class="text-center">
                            <a class="btn btn-primary home-btn" href="/rolunk">
                                <em class="home-botontext">Rólunk</em>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="testimonials">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-lg-5 col-lg-offset-1">
                    <div class="boxes-1">
                        <div class="home-box-txt dark-grey box-2-left">
                            “<span class="dark-blue">A súlyos betegségek könnyen megváltoztathatják az életünket</span>
                            - az Önét és a családjáét is. Tartozik magának azzal,
                            hogy a legjobb szakemberek véleményét válassza.”
                        </div>
                        <div class="home-box-txt-s">
                            <span class="home-box-txt-s-title dark-blue"><strong>Dr Theodore I. Steinman</strong></span>
                            <span class="home-box-txt-s-txt blue">
                            Klinikai Professzor, Harvard Orvosi Egyetem Főorvos,
                            Beth Israel Diakónus Orvosi Központ, Brigham Női Kórház
                        </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box-2-right"></div>
                </div>
            </div>
        </div>
    </section>

    <section id="mapsection">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="boxes">
                        <div id="animaciobox1_hype_container box-3-left">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="home-box-txt dark-grey  box-3-right">
                        <span class="dark-blue">A Teladoc évente több mint 35 millió pácienssel kerül kapcsolatba</span>,
                        és világszerte a legjobbnak tartott szakorvosok tapasztalatát és tudását
                        bocsájtja rendelkezésükre.
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer();
