<?php /* Template Name: Pharma üzletág */ ?>

<?php get_header(); ?>
<section class="subhero-section blue-degree-bg">

    <div class="container">
<!--        <div class="subhero" style="background-image:url(--><?php //echo get_the_post_thumbnail_url() ?>/*)">*/
        <div class="subhero">

            <div class="banner-experts-small">
                <p><?php echo get_field( 'headline' ); ?></p>
            </div>
        </div>
    </div>
</section>
<div class="submenucontainer">
    <div class="container">
        <nav class="submenu">
            <ul class="columns">
                <li class="back-button">
                    <a href="/pharma-uzletag/">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                        Vissza
                    </a>
                </li>
                <?php wp_nav_menu(array(
                        'menu' => 'pharma_uzletag_submenu',
                        'menu_container' => '',
                        'theme_location' => 'pharma_uzletag_submenu',
                        'container' => FALSE,
                        'items_wrap' => '%3$s',
                        'container_class' => false,
                        'container_id' => '',
                        'menu_class' => 'menu',
                        'fallback_cb' => 'wp_page_menu',
                        'before' => '',
                        'after' => '',
                        'link_before' => '',
                        'link_after' => '',
                        'walker' => ''
                    )
                ); ?>
            </ul>
        </nav>
    </div>
</div>
<div class="sensor"></div>
<div class="sensortwo"></div>

<section class="page-custom">
    <div class="container">
        <?php
        // Start the loop.
        while (have_posts()) : the_post(); ?>

            <div class="expert-info-title dark-blue">
                <?php echo get_the_title() ?>
            </div>

            <?php // Include the page content template.
            the_content();

//            // If comments are open or we have at least one comment, load up the comment template.
//            if (comments_open() || get_comments_number()) {
//                comments_template();
//            }

            // End of the loop.
        endwhile;
        ?>
    </div>
</section>

<?php get_footer(); ?>











