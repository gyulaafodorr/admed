<?php get_header() ?>

    <div class="vallalati-egeszseg-hero">
        <div class="overlay"></div>
        <div class="slogen">
            <img src="<?php echo get_template_directory_uri() ?>/img/logo-advance-medical.png"
                 class="advance-medical-logo"
                 alt="Logo Teladoc">

            <p>VÁLLALATI EGÉSZSÉG ÉJJEL-NAPPAL</p>
        </div>
    </div>
    <div class="clearfix vallalati-egeszseg-herobottom"></div>

    <div class="submenucontainer">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <nav class="submenu">
                        <ul class="columns">
                            <li id="menu-item-111"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111">
                                <a href="javascript:void(0);" data-target="vallalati-egeszseg" class="scroll-menu-item">Vállalati
                                    egészség</a>
                            </li>

                            <li id="menu-item-222"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-222">
                                <a href="javascript:void(0);" data-target="vallalati-diagnozis" class="scroll-menu-item">Vállalati
                                    diagnózis</a>
                            </li>

                            <li id="menu-item-333"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-333">
                                <a href="javascript:void(0);" data-target="dolgozoi-asszisztencia" class="scroll-menu-item">Dolgozói
                                    asszisztencia</a>
                            </li>

                            <li id="menu-item-444"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-444">
                                <a href="javascript:void(0);" data-target="kapcsolat" class="scroll-menu-item">Kapcsolat</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="sensor"></div>
    <div class="sensortwo"></div>

    <section id="subpagecontent" class="vallalati-egeszseg-subpagecontent">


        <div class="content-wrapp bg-white">
            <div class="wrapp">
                <div id="expert-infofaq" style="margin-bottom:20px;">
                    <div id="expert-infofaq-txt" class="blue" style="float:none;">
                        <span class="dark-blue">Teladoc Hungary Kft.<br/>Vállalati egészség üzletága</span>
                        <br/><br/>
                        Vállalati egészségprogram, vállalati diagnózis,<br/>egészségfejlesztés, dolgozói asszisztencia
                    </div>
                </div>
                <div style="padding-bottom:40px;">
                    <p style="text-align:justify;">
                        Vállalati partnereink számára összeállítottunk egy komplex megelőző, egészségfejlesztő programot, mely a testi és
                        lelkei egészséggel kapcsolatosan teljes körű támogatást és védelmet nyújt a dolgozóknak. Orvosaink és tanácsadóink
                        éjjel-nappal azonnali segítséget nyújtanak bármely egészséggel, betegséggel kapcsolatos kérdésben, országos
                        partnerhálózatunk révén segítenek gyorsan eljutni a megfelelő szakemberhez, és támogatást nyújtanak a gyógyulás
                        folyamatában, illetve a személyes egészségterv megvalósításában.
                    </p>
                </div>
            </div>
        </div>

        <div id="vallalati-egeszseg" class="content-wrapp blue-title-bg">
            <div class="wrapp wrapp-sep">
                VÁLLALATI EGÉSZSÉG
            </div>
        </div>
        <div class="content-wrapp bg-white">
            <div class="wrapp">
                <div style="padding-top:40px; padding-bottom:40px;">
                    <p style="text-align:justify;">
                        A Teladoc világszerte 35 millió ügyfelet szolgál ki. Vállalati egészségprogramunkhoz a világ vezető
                        nagyvállalati csatlakoztak. Egészségszakértőként elsődleges célunk, hogy partnereink a legjobb szakemberek
                        véleményének segítségével dönthessenek egészségükről. Olyan szakértelem áll rendelkezésünkre, melynek segítségével,
                        integrált módon nyújtunk komplex megoldásokat a vállalati egészség előmozdítására. A vállalati egészség programban a
                        felmérésétől kezdve egészen a közösen kitűzött célok megvalósításig, folyamatos tanácsadást végzünk.
                    </p>
                </div>
            </div>
        </div>

        <div id="vallalati-diagnozis" class="content-wrapp blue-title-bg">
            <div class="wrapp wrapp-sep">
                VÁLLALATI DIAGNÓZIS
            </div>
        </div>
        <div class="content-wrapp bg-white">
            <div class="wrapp">
                <div style="padding-top:40px; padding-bottom:40px;">
                    <p style="text-align:justify;">
                        Orvosok, pszichológusok, dietetikusok szakemberek által összeállított tesztjeinkkel anonim módon felmérjük a
                        dolgozók egészségi állapotát, kockázatait, kritikus egészségi pontokat, és az egyénnek személyekre szabott, a
                        vállalatnak pedig általános képet nyújtunk arról hogy mit kell tenni. Ez a vállalati diagnózis kijelöli az
                        egészfejlesztés területeit, amelyekre célzottan fokuszálhatunk, és amelyek kapcsán a vállalati egészség program
                        megfelelő elemeit összeállíthatjuk. Ennek eredménye, hogy vállalat egyéni sajátságira szabva állítjuk a rájuk a
                        szabott programot, hogy hatékonyan használjuk fel a forrásokat.
                        Emellett különféle egészségmegőrző és betegségmenedzsment programokat is működtetünk, amelyek az egészséges
                        életmódra vágyó vagy betegséggel élő dolgozókat segítik. Bármely témában tartunk egészségmegőrző,  testi vagy akár
                        lelki egészséggel kapcsolatos előadásokat.
                    </p>
                </div>
            </div>
        </div>

        <div id="dolgozoi-asszisztencia" class="content-wrapp blue-title-bg">
            <div class="wrapp wrapp-sep">
                DOLGOZÓI ASSZISZTENCIA
            </div>
        </div>
        <div class="content-wrapp bg-white">
            <div class="wrapp">
                <div style="padding-top:40px; padding-bottom:40px;">
                    <p style="text-align:justify;">
                        A dolgozók egészsége minden vállalat számára kulcsfontosságú kérdés, hiszen a vállalat sikeres működéséhez
                        elengedhetetlen az elégedett és egészséges munkavállaló.
                        A dolgozói asszisztencia program integrált megoldást nyújt a vállalatok számára, melynek keretében a munkavállalók
                        azonnali segítséget kaphatnak a legjobb szakemberektől a lelkk vagy testi egészséggel kapcsolatos bármely kérdésben.
                    </p>
                    <p style="text-align:justify; margin-top:20px;">
                        A program telefonon és online elérhető pszichológiai, dietetikai és orvosi tanácsadást, pszichológiai és egészségi
                        teszteket illetve állapotfelmérést, orvosi ellátásszervezést és számos magánklinikán egyedi díjkedvezményt nyújt a
                        munkavállalóknak. Ezeken túl végzünk helyszíni diétás és edzői tanácsadást, fittség felmérést, személyes
                        egészségfejlesztés. A szolgáltatásaink tehát egy komplex vállalti egészségprogram keretében minden igényre
                        válaszolnak a megelőzéstől a kezelésig. Az online tesztjeink segítségével nem csak az egyén, de a vállalat számára
                        is diagnózist tudunk adni a dolgozók fizikális és mentális állapotáról, és meghatározhatjuk, hogy mely témákban,
                        mely területeken kell ez egészségfejlesztés eszközeivel leginkább beavatkozni. Szolgáltatásunk lényege az állandó
                        rendelkezésre állás, a tanácsadás, a dolgozók folyamatos tájékoztatása és segítése, a megelőzés az egészséges
                        életmód támogatása.
                    </p>
                </div>
            </div>
        </div>

        <div id="kapcsolat" class="content-wrapp blue-title-bg">
            <div class="wrapp wrapp-sep">
                KAPCSOLAT
            </div>
        </div>
        <div class="content-wrapp bg-white">
            <div class="wrapp">
                <div style="padding-top:40px; padding-bottom:40px;">
                    <p style="text-align:center; font-size:20px;">
                        <a href="mailto:eap@advance-medical.hu">eap@advance-medical.hu</a>
                    </p>
                </div>
            </div>
        </div>


    </section>


<?php get_footer() ?>
