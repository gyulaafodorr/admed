<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage admed
 * @since admed 8.6
 */

?><!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Teladoc</title>

    <!-- Bootstrap -->
    <link href="<?php bloginfo('template_url'); ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link rel="shortcut icon" href="/img/favicon-adbance-medical2.ico"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700,800&amp;subset=latin,latin-ext"
            rel="stylesheet" type="text/css">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <META NAME="ROBOTS" CONTENT="INDEX, FOLLOW">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--[if lte IE 9]>
    <script type="text/javascript">
        document.createElement("nav");
        document.createElement("header");
        document.createElement("footer");
        document.createElement("section");
        document.createElement("article");
        document.createElement("aside");
        document.createElement("hgroup");
    </script>
    <![endif]-->

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,300italic' rel='stylesheet' type='text/css'/>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77317209-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {dataLayer.push(arguments);}

        gtag('js', new Date());
        gtag('config', 'UA-109680131-1');
    </script>

    <script>
        provider_JSON_url = '<?php echo PROVIDER_JSON_URL ?>';
    </script>
    <?php wp_head(); ?>
</head>

<body <?php body_class() ?>>
<header>
    <nav class="navbar navbar-admed affix-top" id="navbar-admed">

        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo home_url() ?>" style="background-image:url('<?php bloginfo(
                    'template_url'
                ); ?>/img/logo-teladoc-horiz.png');background-repeat: no-repeat;background-size: contain;"></a>
                <!--                                        <a class="navbar-brand" href="#"><h2 style="color: #ffffff">advance|<em>medical</em></h2></a>-->
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="https://teladochealth.com/en/" class="fa fa-globe"></a>
                    </li>
                    <li>
                        <a href="/kapcsolat" class="fa fa-phone"></a>
                    </li>
                </ul>
                <?php wp_nav_menu(
                    array(
                        'theme_location' => 'header_menu',
                        'menu' => 'header_menu',
                        'menu_container' => 'ul',
                        'menu_class' => 'nav navbar-nav left',
                    )
                ); ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>

<div class="container">
    <div class="row">
        <a href="#top">
            <div class="scrolltop-wrapp">
                <div class="scrolltop"></div>
                <div class="scrolltop-roll"></div>
            </div>
        </a>
    </div>
</div>


