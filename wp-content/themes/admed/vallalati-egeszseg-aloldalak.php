<?php /* Template Name: Vállalati egészség aloldalak */ ?>

<?php get_header(); ?>

<style type="text/css">
    .percent-txt {
        margin-top: 30px;
        line-height: 180%;
    }
</style>

<section class="subhero-section blue-degree-bg">

    <div class="container">
        <div class="row">
            <div class="subhero" style="background-image:url(<?php echo get_the_post_thumbnail_url() ?>)">
                <div class="banner-experts-small">
                    <p><?php echo get_field( 'headline' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="submenucontainer">
    <div class="container">
        <nav class="submenu">
            <ul class="columns">
                <li class="back-button">
                    <a href="/vallalati-egeszseg-uzletag/">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                        Vissza
                    </a>
                </li>
				<?php wp_nav_menu( array(
						'menu'            => 'vallalati_egeszseg_submenu',
						'menu_container'  => '',
						'theme_location'  => 'vallalati_egeszseg_submenu',
						'container'       => false,
						'items_wrap'      => '%3$s',
						'container_class' => false,
						'container_id'    => '',
						'menu_class'      => 'menu',
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'walker'          => ''
					)
				); ?>
            </ul>
        </nav>
    </div>
</div>
<div class="sensor"></div>
<div class="sensortwo"></div>

<section class="page-custom">
    <div class="container-fluid">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			the_content();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>
    </div>
</section>

<?php get_footer(); ?>











