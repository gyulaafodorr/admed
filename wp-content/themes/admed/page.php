<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage admed
 * @since admed 8.6
 */

get_header(); ?>
<div class="subhero">
    <div class="banner-experts-small">
        <p><?php echo get_field( 'headline' ); ?></p>
    </div>
</div>
<section class="page">
    <div class="container-fluid">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            the_content();

            // If comments are open or we have at least one comment, load up the comment template.
            if (comments_open() || get_comments_number()) {
                comments_template();
            }

            // End of the loop.
        endwhile;
        ?>
    </div>
</section>

<?php get_footer(); ?>
