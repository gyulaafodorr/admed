<?php
/**
 * Admed functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 *
 * @package WordPress
 * @subpackage admed
 * @since admed 8.6
 */

//custom menu
register_nav_menus(array(
    'header_menu' => 'Header menu',
    'header_menu_right' => 'Header menu right',
    'nemzetközi_orvosi_szakvelemeny_submenu' => 'Nemzetközi orvosi szakvelemeny submenu',
    'pharma_uzletag_submenu'=>'Pharma uzletag submenu',
    'vallalati_egeszseg_submenu'=>'Vallalati egeszseg submenu',
    'footer_rolunk_kapcsolat'=>'Footer rolunk kapcsolat'
));


add_filter('xmlrpc_enabled', '__return_false');