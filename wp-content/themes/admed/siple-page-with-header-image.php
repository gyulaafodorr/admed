<?php /* Template Name: Simple page with header image */ ?>

<?php get_header(); ?>
<section class="subhero-section blue-degree-bg">

    <div class="container">
        <div class="row">
            <div class="subhero">

                <div class="banner-experts-small">
                    <p><?php echo get_field( 'headline' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="page-custom">
    <div class="container-fluid">
        <?php
        // Start the loop.
        while (have_posts()) : the_post();

            // Include the page content template.
            the_content();

            // If comments are open or we have at least one comment, load up the comment template.
            if (comments_open() || get_comments_number()) {
                comments_template();
            }

            // End of the loop.
        endwhile;
        ?>
    </div>
</section>

<?php get_footer(); ?>











