<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage admed
 * @since admed 8.6
 */

get_header(); ?>
<section class="post-single">
    <div class="container">
        <?php
        // Start the loop.
        while (have_posts()) : the_post(); ?>
            <div class="expert-info-title dark-blue">
                <?php echo get_the_title() ?>
            </div>
            <?php the_content() ?>

        <?php endwhile;
        ?>
    </div>
</section>
<?php get_footer() ?>
