<?php get_header() ?>
    <section id="maphero">
        <div id="map_canvas">
        </div>
    </section>
    <section id="partner">
        <div class="container">
            <div class="partners">
                <div class="maincontent">
                    <br>
                    <br>

                    <div class="expert-info-title dark-blue" style="width: 100% !important;">
                        ORSZÁGOS HÁLÓZATUNK
                    </div>

                    <p>Az  2008 óta folyamatosan fejleszti egészségügyi szolgáltatókból álló
                        partnerhálózatát, mely az ország minden megyéjében, közel 400 intézményben, 60
                        településen
                        több ezer szakorvos tudásához kínál hozzáférést.
                    </p>
                    <br>
                    <br>
                    <br>
                    <br>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-md-6">
                <div class="expert-info-title dark-blue" style="width: 100% !important;">Partnerhálózatunk révén
                    számtalan
                    egészségügyi szolgáltatáshoz nyújtunk
                    hozzáférést:
                </div>
                - Járóbeteg szakellátás minden szakmában (pl. belgyógyászat, bőrgyógyászat,csecsemő- és
                gyermekgyógyászat, diabetológia, endokrinológia, érsebészet, fül-orr-gégészet,
                gastroenterológia, kardiológia, neurológia, nőgyógyászat, ortopédia, sebészet, szemészet,
                tüdőgyógyászat, urológia stb.)
                - Diagnosztika (ultrahang, röntgen, labor vizsgálatok stb.)
                <br>
                - Egy napos sebészeti beavatkozások
                <br>
                - Több napos kórházi kezelés, műtétek
                <br>
                - Nagy értékű diagnosztika (CT, MRI, PET CT, Cardio CT)
                <br>
                - Betegszállítás
                <br>
                - Otthonápolás
                <br>
                - Kórházi extra szolgáltatások
            </div>
            <div class="col-md-6">
                <div class="expert-info-title dark-blue" style="width: 100% !important;">Csatlakozzon
                    hozzánk, legyen partnerünk a gyógyításban
                </div>

                <p>Egészségügyi szolgáltatókból, vagyis kórházakból, magánklinikákból,
                    magán orvosi rendelőkből álló hálózatunkat folyamatosan fejlesztjük. A szolgáltató
                    partnerekkel történő együttműködés a kölcsönös előnyökön alapszik. Partnereink
                    általunk növelhetik a betegkörüket, míg a Teladoc az egyre nagyobb
                    választási lehetőséget biztosító partnerhálózat révén megfelelő minőségű, és
                    földrajzilag is elérhető egészségügyi szolgáltatást tud az ügyfelei rendelkezésére
                    bocsátani.</p>

                <p>Amennyiben Ön vagy a vállalkozása orvosi szolgáltatásokat nyújt, és
                    szívesen együttműködne velünk, kérjük, töltse ki az alábbi űrlapot. Az adatai
                    megadásával bekerül adatbázisunkba, melyet kizárólag az ügyfeleink számára szükséges
                    orvosi ellátások megszervezésre használnunk. Kéretlen, netán reklámcélú levelet soha
                    sem küldünk.</p>

                <p>Bízunk abban, hogy Önt és klinikáját/rendelőjét hamarosan partnereink
                    között tarthatjuk számon.</p>
            </div>
        </div>
    </section>
    <section id="ninjaform">
        <div class="container">
            <div class="col-md-6 col-md-offset-3">
                <div class="row">
                    <div class="ninjaform">
                        <?php echo do_shortcode("[ninja_form id=1]") ?>
                    </div>
                </div>
            </div>
    </section>
<?php get_footer() ?>
