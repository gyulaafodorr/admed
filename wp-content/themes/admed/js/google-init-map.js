// MAP
function initMap() {
    var markersForGroup = [];
    var markers = [];
    var infoWindows = [];

    var clusterStyles = [
        {
            textColor: 'transparent',
            textSize: 14,
            url: '/wp-content/themes/admed/img/teladochealth_logo_o.png',
            height: 25,
            width: 25
        }
    ];
    var mcOptions = { gridSize: 20, maxZoom: 15, styles: clusterStyles };
    var styledMapType = new google.maps.StyledMapType(
      [
          {
              'featureType': 'landscape.natural',
              'stylers': [
                  {
                      'visibility': 'off'
                  }
              ]
          },
          {
              'featureType': 'poi.park',
              'stylers': [
                  {
                      'visibility': 'off'
                  }
              ]
          }
      ],
      { name: 'SyledMap' }
    );

    var mapOptions = {
        scrollwheel: false,
        center: new google.maps.LatLng(47.6, 19.5),
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');

    // place markers
    $.getJSON(provider_JSON_url, function (json1) {
        $.each(json1, function (key, data) {
            var latLng = new google.maps.LatLng(data.lat, data.lng);
            var finalLatLng = latLng;

            if (markers.length != 0) {
                for (var mkey in markers) {
                    var existingMarker = markers[mkey];
                    var pos = existingMarker.getPosition();

                    //if a marker already exists in the same position as this marker
                    if (latLng.equals(pos)) {
                        //update the position of the coincident marker by applying a small multipler to its coordinates
                        var newLat = latLng.lat() + (Math.random() - .5) / 1500;// * (Math.random() * (max - min) + min);
                        var newLng = latLng.lng() + (Math.random() - .5) / 1500;// * (Math.random() * (max - min) + min);
                        finalLatLng = new google.maps.LatLng(newLat, newLng);
                    }
                }
            }

            var marker = new google.maps.Marker({
                position: finalLatLng,
                title: data.name,
                providerId: data.id,
                map: map,
                icon: '/wp-content/themes/admed/img/teladochealth_logo_o.png',
                //label: '1'
            });

            var infos = '<div style=\'padding:10px;\'>' +
              '<b>' + data.name + '</b>' +
              '<br />' +
              data.city + data.address +
              '<br />' +
              // "<img src='data:image/jpg;base64," + data.logo + "' style='max-width:100px;'/>" +
              '</div>';

            var infoWindow = new google.maps.InfoWindow();
            infoWindow.setContent(infos);

            marker.addListener('mousemove', function () {
                infoWindow.open(map, marker);
            });
            marker.addListener('mouseout', function () {
                infoWindow.close();
            });

            infoWindows[data.id] = infoWindow;
            markers[data.id] = marker;
            markersForGroup.push(marker);
        });

        mc = new MarkerClusterer(map, markersForGroup, mcOptions);

    }).fail(function () {
        console.log('Hiba a szolgáltatók lekérdezése során.');
    });
}
