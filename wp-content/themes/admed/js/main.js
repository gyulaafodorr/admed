$('#navbar-admed').affix({
    offset: {
        top: 5,
        bottom: function () {
            return (this.bottom = $('.footer').outerHeight(true))
        }
    }
});

//submenu fixed under topmenu
$(window).scroll(function () {
    var scont = '.submenucontainer';
    if ($(scont).length) {
        var sr = '.sensor';
        var srtwo = '.sensortwo';
        var smpos = $(scont).offset().top - $(window).scrollTop();
        var srtwopos = $(srtwo).offset().top - $(window).scrollTop();
        if (smpos < 64) {
            $(scont).addClass('smfxt');
            $(sr).addClass('sensoractive');
        }

        if (srtwopos > 124) {
            $(scont).removeClass('smfxt');
            $(sr).removeClass('sensoractive');
        }
    }
});

$.fn.scrollToVallalatiEgeszseg = function (speed) {
    if (typeof(speed) === 'undefined')
        speed = 1000;

    $('html, body').animate({
        scrollTop: parseInt($(this).offset().top - 100)
    }, speed);
};

$(document).ready(function () {
    $('.scroll-menu-item').on('click', function (e) {
        var element = $(e.target);
        var target = element.data('target');

        $('#' + target).scrollToVallalatiEgeszseg();
    });
});
