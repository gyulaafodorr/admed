set :application, "admedwp"
set :deploy_user, 'admedwp'

set :repo_url, 'git@bitbucket.org:gyulaafodorr/admed.git'

append :linked_files, "wp-config.php"
append :linked_dirs, "wp-content/uploads"

set :keep_releases, 5
